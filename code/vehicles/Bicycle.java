package vehicles;
//Karekin Kiyici 1932233
public class Bicycle{
    
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int gears, double speed){
        this.manufacturer = manufacturer;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + "; Number of Gears: " + this.numberGears + "; Max speed: " + this.maxSpeed;
    }
}