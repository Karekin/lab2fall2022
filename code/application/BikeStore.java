package application;
//Karekin Kiyici 1932233
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Ford", 6, 38);
        bikes[1] = new Bicycle("Honda", 5, 26);
        bikes[2] = new Bicycle("BikePro", 4, 40);
        bikes[3] = new Bicycle("EasyHike", 8, 31);
        
        for(int i = 0; i < bikes.length; i++){

            System.out.println(bikes[i]);
        }
    }
}
